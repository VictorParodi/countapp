import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  final bodyText = new TextStyle(fontSize: 25.0);

  @override
  Widget build(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CountApp'),
      ),

      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Tabs numbers:', style: bodyText),
            Text('0', style: bodyText),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => print('Awesome!'),
      ),
    );
  }
}