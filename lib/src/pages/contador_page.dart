import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  @override
  createState() => new _ContadorPageState();
}

class _ContadorPageState extends State<ContadorPage> {
  final _bodyText = new TextStyle(fontSize: 25.0);
  int _count = 0;

  @override
  Widget build(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CountApp'),
      ),

      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Tabs numbers:', style: _bodyText),
            Text('$_count', style: _bodyText),
          ],
        ),
      ),
      floatingActionButton: _createButtons(),
    );
  }

  Widget _createButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(width: 30.0,),

        FloatingActionButton(
          child: Icon(Icons.exposure_zero),
          onPressed: _reset,
        ),

        Expanded(child: SizedBox(),),

        FloatingActionButton(
          child: Icon(Icons.remove),
          onPressed: _takeaway,
        ),

        SizedBox(width: 10.0,),

        FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: _add,
        ),
      ],
    );
  }

  void _add() {
    _count++;
    setState(() {});
  }

  void _takeaway() {
    _count--;
    setState(() {});
  }

  void _reset() {
    _count = 0;
    setState(() {});
  }
}